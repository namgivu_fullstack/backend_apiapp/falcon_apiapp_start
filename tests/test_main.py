import pytest
from falcon import testing
from app.main import app

@pytest.fixture
def client():
    return testing.TestClient(app)

def test_hello_world(client):
    result = client.simulate_get('/hello')
    assert result.status_code == 200
    assert result.json == {'message': 'Hello, World!'}

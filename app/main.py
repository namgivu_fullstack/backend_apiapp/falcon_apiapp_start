import falcon

class HelloWorldResource:
    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200
        resp.media = {'message': 'Hello, World!'}

app = falcon.API()
app.add_route('/hello', HelloWorldResource())
